{
    "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
        "adminUsername": {
            "type": "string"
        },
        "adminPassword": {
            "type": "string"
        },
        "practiceName": {
            "type": "string"
        },
        "interviewerName": {
            "type": "string"
        },
        "imageRG": {
            "type": "string"
        },
        "vmImage": {
            "type": "string"
        },
        "autoShutdownStatus": {
            "type": "string"
        },
        "autoShutdownTime": {
            "type": "string"
        },
        "autoShutdownTimeZone": {
            "type": "string"
        },
        "autoShutdownNotificationStatus": {
            "type": "string"
        },
        "autoShutdownNotificationEmail": {
            "type": "string"
        },
        "scriptFileUri": {
            "type": "string"
        },
        "branchName": {
            "type": "string"
        },
        "repoName": {
            "type": "string"
        },
        "gitPat": {
            "type": "string"
        },
        "saName": {
            "type": "string"
        },
        "saKey": {
            "type": "string"
        },
        "fileName": {
            "type": "string"
        },
        "subId": {
            "type": "string"
        },
        "imageRef": {
            "type": "string"
        }
    },
    "variables": {
        "username": "[if(greater(length(parameters('adminUsername')), 7), substring(parameters('adminUsername'), 0, 5), parameters('adminUsername'))]",
        "resourcePrefix": "[parameters('practiceName')]",
        "location": "eastus",
        "subscriptionRef": "[concat('/subscriptions/', parameters('subId'))]",
        "networkInterfaceName": "[concat(variables('resourcePrefix'), '-', variables('username'), '-ni')]",
        "publicIpName": "[concat(variables('resourcePrefix'), '-', parameters('adminUsername'), '-ip')]",
        "vmName": "[concat(variables('resourcePrefix'), '-', variables('username'))]",
        "vnetName": "neugs-codetest-rg-vnet",
        "nsgName": "[concat(variables('resourcePrefix'), '-', variables('username'), '-nsg')]",
        "nsgId": "[resourceId(resourceGroup().name, 'Microsoft.Network/networkSecurityGroups', variables('nsgName'))]",
        "vnetId": "[resourceId(resourceGroup().name, 'Microsoft.Network/virtualNetworks', variables('vnetName'))]",
        "subnetRef": "[concat(variables('vnetId'), '/subnets/default')]",
        "imageRef": "[concat(variables('subscriptionRef'), '/resourceGroups/', parameters('imageRG'), '/providers/Microsoft.Compute/galleries/', parameters('imageRef'))]"
    },
    "resources": [
        {
            "name": "[variables('networkInterfaceName')]",
            "type": "Microsoft.Network/networkInterfaces",
            "apiVersion": "2019-07-01",
            "location": "[variables('location')]",
            "dependsOn": [
                "[concat('Microsoft.Network/networkSecurityGroups/', variables('nsgName'))]"
            ],
            "properties": {
                "ipConfigurations": [
                    {
                        "name": "ipconfig1",
                        "properties": {
                            "subnet": {
                                "id": "[variables('subnetRef')]"
                            },
                            "privateIPAllocationMethod": "Dynamic",
                            "publicIpAddress": {
                                "id": "[resourceId(resourceGroup().name, 'Microsoft.Network/publicIpAddresses', variables('publicIpName'))]"
                            }
                        }
                    }
                ],
                "networkSecurityGroup": {
                    "id": "[variables('nsgId')]"
                }
            },
            "tags": {
                "Practice": "[parameters('practiceName')]",
                "Interviewer": "[parameters('interviewerName')]",
                "Candidate": "[parameters('adminUsername')]"
            }
        },
        {
            "name": "[variables('nsgName')]",
            "type": "Microsoft.Network/networkSecurityGroups",
            "apiVersion": "2019-02-01",
            "location": "[variables('location')]",
            "properties": {
                "securityRules": [
                    {
                        "name": "RDP",
                        "properties": {
                            "priority": 300,
                            "protocol": "TCP",
                            "access": "Allow",
                            "direction": "Inbound",
                            "sourceAddressPrefix": "*",
                            "sourcePortRange": "*",
                            "destinationAddressPrefix": "*",
                            "destinationPortRange": "3389"
                        }
                    }
                ]
            },
            "tags": {
                "Practice": "[parameters('practiceName')]",
                "Interviewer": "[parameters('interviewerName')]",
                "Candidate": "[parameters('adminUsername')]"
            }
        },
        {
            "name": "[variables('vmName')]",
            "type": "Microsoft.Compute/virtualMachines",
            "apiVersion": "2019-07-01",
            "location": "[variables('location')]",
            "dependsOn": [
                "[concat('Microsoft.Network/networkInterfaces/', variables('networkInterfaceName'))]"
            ],
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_B2ms"
                },
                "storageProfile": {
                    "osDisk": {
                        "createOption": "fromImage",
                        "managedDisk": {
                            "storageAccountType": "Standard_LRS"
                        }
                    },
                    "imageReference": {
                        "id": "[variables('imageRef')]"
                    }
                },
                "networkProfile": {
                    "networkInterfaces": [
                        {
                            "id": "[resourceId('Microsoft.Network/networkInterfaces', variables('networkInterfaceName'))]"
                        }
                    ]
                },
                "osProfile": {
                    "computerName": "[variables('vmName')]",
                    "adminUsername": "[parameters('adminUsername')]",
                    "adminPassword": "[parameters('adminPassword')]",
                    "windowsConfiguration": {
                        "enableAutomaticUpdates": true,
                        "provisionVmAgent": true
                    }
                }
            },
            "tags": {
                "Practice": "[parameters('practiceName')]",
                "Interviewer": "[parameters('interviewerName')]",
                "Candidate": "[parameters('adminUsername')]"
            }
        },
        {
            "name": "[concat('shutdown-computevm-', variables('vmName'))]",
            "type": "Microsoft.DevTestLab/schedules",
            "apiVersion": "2017-04-26-preview",
            "location": "[variables('location')]",
            "dependsOn": [
                "[concat('Microsoft.Compute/virtualMachines/', variables('vmName'))]"
            ],
            "properties": {
                "status": "[parameters('autoShutdownStatus')]",
                "taskType": "ComputeVmShutdownTask",
                "dailyRecurrence": {
                    "time": "[parameters('autoShutdownTime')]"
                },
                "timeZoneId": "[parameters('autoShutdownTimeZone')]",
                "targetResourceId": "[resourceId('Microsoft.Compute/virtualMachines', variables('vmName'))]",
                "notificationSettings": {
                    "status": "[parameters('autoShutdownNotificationStatus')]",
                    "notificationLocale": "en",
                    "timeInMinutes": "30",
                    "emailRecipient": "[parameters('autoShutdownNotificationEmail')]"
                }
            },
            "tags": {
                "Practice": "[parameters('practiceName')]",
                "Interviewer": "[parameters('interviewerName')]",
                "Candidate": "[parameters('adminUsername')]"
            }
        },
        {
            "name": "[concat(variables('vmName'),'/CustomScriptExtension')]",
            "type": "Microsoft.Compute/virtualMachines/extensions",
            "location": "[variables('location')]",
            "apiVersion": "2015-06-15",
            "properties": {
              "publisher": "Microsoft.Compute",
              "type": "CustomScriptExtension",
              "typeHandlerVersion": "1.9",
              "autoUpgradeMinorVersion": true,
              "settings": {
                "fileUris": "[split(parameters('scriptFileUri'), ' ')]"
              },
              "protectedSettings": {
                "commandToExecute": "[concat ('powershell -ExecutionPolicy Unrestricted -File ', parameters('fileName'), ' ', parameters('branchName'), ' ', parameters('repoName'), ' ', parameters('gitPat'))]",
                "storageAccountName": "[parameters('saName')]",
                "storageAccountKey": "[parameters('saKey')]"
              }
            },
            "dependsOn": [
                "[concat('Microsoft.Compute/virtualMachines/', variables('vmName'))]"
            ]
        }
    ]
}
